package com.izulrad.ssc.view.dialog.constant;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.izulrad.ssc.R;
import com.izulrad.ssc.util.ConstantUtils;

public class AddConstantDialog extends DialogFragment {
    public static final String TAG = AddConstantDialog.class.getName();
    private static final String CONSTANT_VALUE = "constantValue";

    public static DialogFragment newInstance(@Nullable String constantValue) {
        DialogFragment result = new AddConstantDialog();

        Bundle args = new Bundle();
        args.putString(CONSTANT_VALUE, constantValue);
        result.setArguments(args);

        return result;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_constant_add, null);
        final EditText edConstantName = (EditText) view.findViewById(R.id.dialog_constant_add_name);
        final EditText edConstantValue = (EditText) view.findViewById(R.id.dialog_constant_add_value);

        String constantValue = getArguments().getString(CONSTANT_VALUE);
        if (constantValue != null) {
            edConstantValue.setText(constantValue);
        }

        final AlertDialog mDialog = new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(getString(R.string.dialog_constant_add_title))
                .setPositiveButton(getString(R.string.ok), null)
                .setNegativeButton(getString(R.string.cancel), null)
                .create();

        mDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnOk = mDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        boolean verifyName = ConstantUtils.verify(getActivity(), edConstantName, true);
                        boolean verifyValue = ConstantUtils.verify(getActivity(), edConstantValue, false);
                        if (!verifyName || !verifyValue) return;

                        String constantName = edConstantName.getText().toString();
                        String constantValue = edConstantValue.getText().toString();
                        ConstantUtils.addNewConstant(constantName, constantValue);

                        Fragment targetFragment = getTargetFragment();
                        if (targetFragment != null) {
                            targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                        }

                        mDialog.dismiss();
                    }
                });
            }
        });

        return mDialog;
    }
}
