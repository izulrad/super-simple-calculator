package com.izulrad.ssc.calculate.imp;

import com.izulrad.ssc.calculate.AbstractExpressionCalculateTest;
import com.izulrad.ssc.calculate.FunctionCalculate;
import com.izulrad.ssc.calculate.OperationCalculate;

import org.junit.Before;

public class RecursiveExpressionCalculateTest extends AbstractExpressionCalculateTest {

  @Before
  public void setUp() throws Exception {
    diff = 0.000001;
    OperationCalculate opCalc = new SimpleOperationCalculate();
    FunctionCalculate fuCalc = new SimpleFunctionCalculate();
    expressionCalculate = new RecursiveExpressionCalculate(opCalc, fuCalc);
  }
}