package com.izulrad.ssc.calculate;

public interface OperationCalculate {
    String operation(String leftOperand, String rightOperand, String operator);
}
