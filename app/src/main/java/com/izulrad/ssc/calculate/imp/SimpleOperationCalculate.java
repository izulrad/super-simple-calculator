package com.izulrad.ssc.calculate.imp;

import com.izulrad.ssc.calculate.CalculateUtils;
import com.izulrad.ssc.calculate.OperationCalculate;
import com.izulrad.ssc.calculate.exception.OperatorNotDefinedException;
import com.izulrad.ssc.util.Log;

public class SimpleOperationCalculate implements OperationCalculate {
    private static final String TAG = SimpleOperationCalculate.class.getName();

    public String operation(String leftOperand, String rightOperand, String operator) {
        double result,
                x = Double.parseDouble(CalculateUtils.openParentheses(leftOperand)),
                y = Double.parseDouble(CalculateUtils.openParentheses(rightOperand));

        switch (operator) {
            case "^":
                result = Math.pow(x, y);
                break;
            case "*":
                result = x * y;
                break;
            case "/":
                result = x / y;
                break;
            case "+":
                result = x + y;
                break;
            case "-":
                result = x - y;
                break;

            default:
                throw new OperatorNotDefinedException(String.format("operator %s is not defined", operator));
        }

        Log.d(TAG, String.format("operation %s %s %s = %s", x, operator, y, result));
        return CalculateUtils.doubleToString(result);
    }
}
