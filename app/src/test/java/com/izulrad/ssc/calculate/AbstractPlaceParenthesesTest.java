package com.izulrad.ssc.calculate;

import com.izulrad.ssc.util.Log;
import com.izulrad.ssc.util.UserSetting;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

abstract public class AbstractPlaceParenthesesTest {

  protected PlaceParentheses placeParentheses;

  /**
   * Sequence testing data: expression[0] delimiter("=) expected[1]
   */
  private String[] expressions = {
      "-2+1=(-2)+1",
      "1-2+3=1-2+3",
      "1-2+-3*-2+(-2)*-6=1-2+((-3)*(-2))+((-2)*(-6))",
      "2(1+1)=2*(1+1)",
      "(1+1)(1+1)=(1+1)*(1+1)",
      "-2^(1+1)=-(2^(1+1))",
      "-2^(1+1)+5^3=(-(2^(1+1)))+(5^3)",
      "2^2=2^2",
      "(-2+1)^2=((-2)+1)^2",
      "(-2+1)=(-2)+1",
      "2+-2=2+(-2)",
      "2-2=2-2",
      "7-(-2)=7-(-2)",
      "-2+8=(-2)+8",
      "-2^2=-(2^2)",
      "-(1+1)^2=-((1+1)^2)",
      "-(1+1)^(1+1)=-((1+1)^(1+1))",
      "5-(1+1)^2=5-((1+1)^2)",
      "-(-2)^-2=-((-2)^(-2))",
      "-2^2+1=(-(2^2))+1",
      "-2^(1+1)+5^3-(-2)^(-2)=(-(2^(1+1)))+(5^3)-((-2)^(-2))",
      "sin2=sin(2)",
      "sin-2.0=sin(-2.0)",
      "2sin3=2sin(3)",
      "-2sin(-2)=(-2)sin(-2)",
      "-sin(1.001+1)=-sin(1.001+1)",
      "--(-(-2))=-(-(-(-2)))",
      "----2=-(-(-(-2)))",
      "-(-(-(-2)))+1=(-(-(-(-2))))+1",
      "5+-(2+1)=5+(-(2+1))",
      "-(2+2)+1=(-(2+2))+1",
      "-(sin(2)+1)=-(sin(2)+1)",
      "5----2=5-(-(-(-2)))",
      "-1--1=(-1)-(-1)",
      "(2)+1=2+1",
      "1-(-(1))=1-(-1)",
      "15*(5)-(-(2))=(15*5)-(-2)",
      "-483.95438914^50=-(483.95438914^50)",
      "2*2+2=(2*2)+2",
      "2*2=2*2",
      "(2+(2))=2+2",
      "15*(5)-(-(1))=(15*5)-(-1)",
      "2/2/2+1=((2/2)/2)+1",
      "2/2/2=(2/2)/2",
      "2*2*2/3=(2*2*2)/3",
      "2/2*2+1=((2/2)*2)+1",
      "2/2*2*2/2=((2/2)*2*2)/2",
      "1+2/2*2*2/2=1+(((2/2)*2*2)/2)",
      "((2*2*2)/3)+1+2/2*2*2/2=((2*2*2)/3)+1+(((2/2)*2*2)/2)",
      "2*2*2/3+1+2/2*2*2/2=((2*2*2)/3)+1+(((2/2)*2*2)/2)",
  };

  @Test
  public void testPlace() throws Exception {
    if (placeParentheses == null)
      throw new NullPointerException("placeParentheses == null, use setUp method");
    UserSetting.loadDefault();
    Log.setDebugMode(false);

    for (String s : expressions) {
      String[] arr = s.split("=");
      String expression = arr[0];
      String expected = arr[1];
      String actual = placeParentheses.place(expression);

      assertEquals("it's should be equals", expected, actual);
    }

  }
}
