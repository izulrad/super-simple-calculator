package com.izulrad.ssc.calculate;

public interface PlaceParentheses {
    String place(String expression);
}
