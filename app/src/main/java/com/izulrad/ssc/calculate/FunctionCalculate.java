package com.izulrad.ssc.calculate;

public interface FunctionCalculate {
    String function(String func, String value);
}
