package com.izulrad.ssc.calculate.exception;

public class FunctionNotDefinedException extends RuntimeException {
    public FunctionNotDefinedException(String detailMessage) {
        super(detailMessage);
    }
}
