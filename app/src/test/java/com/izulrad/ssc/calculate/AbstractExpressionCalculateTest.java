package com.izulrad.ssc.calculate;

import com.izulrad.ssc.util.Log;
import com.izulrad.ssc.util.UserSetting;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

abstract public class AbstractExpressionCalculateTest {
  protected ExpressionCalculate expressionCalculate;
  protected double diff;

  /**
   * Sequence the testing data: expression[0] expected[1]
   * Split of "="
   */
  private String[] strings = {
      "2.1+1=3.1",
      "1+1=2",
      "1-1=0",
      "(1+1)*2=4",
      "(2-2)*2=0",
      "(1+1)*(2+2)=8",
      "((1+1)*2)+(2-1)=5",
      "(-1)+1=0",
      "(-1)-1=-2",
      "((-1)-1)-1=-3",
      "1+(-1)+1=1",
      "(1+(-1))=0",
      "cos(3+(19*3))=-0.95241298041",
      "(-(-(-2)))=-2",
      "-(2+3)=-5",
      "(-(-2))-2=0",
      "-(-2)=2",
      "sin(2)=0.90929742682",
      "-sin((2*((-5)+(1.5*4)))+28)=0.98803162409",
      "(0.4/8)=0.05",
      "((2*(12^(-1)))^((2^(-2))+((0.4*1)/8)))+((0.6^4)*(2-(5^2)))=-2.39660931893",
      "(3.0003/(1.00001+(0.5^1)))*4=8.00074666169",
      "(99*99)/(2^(3*(2^2)))=2.39282226563",
      "2*(589+(((2454*0.1548)/0.01)*((-2)+(9^2))))=6003269.36",
      "(-2)-(-2)=0",
      "(-2)-2-(-2)-2=-4",
      "((-2)-1-(-2)-(-2)-((-2)-2-(-2)-2)-2-2)=1",
      "(-2)-((-2)-1-(-2)-(-2)-((-2)-2-(-2)-2)-2-2)=-3",
      "(2+2)=4",
      "(-5*2)=-10",
      "2-1*3=-1",
      "40-65-5*2=-35",
      "30*2+23*5-3*8=151",
      "40-65-5*2+30*2+23*5-3*8=116",
      "cos(1+(-1)+sin(3+2))=0.57440087919",
      "2.*2=4",
      "5-(-(-(-2)))=7",
      "15*5-(-1)=76"
  };

  @Test
  public void testCalculate() throws Exception {
    if (expressionCalculate == null)
      throw new NullPointerException("expressionCalculate == null, use setUp method");
    UserSetting.loadDefault();
    Log.setDebugMode(false);

    for (String s : strings) {
      String[] arr = s.split("=");
      String expression = arr[0];
      Double expected = Double.valueOf(arr[1]);
      Double actual = Double.valueOf(expressionCalculate.calculate(expression));

      assertEquals(String.format("expression %s = ?", expression), expected, actual, diff);
    }
  }
}
