package com.izulrad.ssc.calculate.exception;

public class OperatorNotDefinedException extends RuntimeException {
    public OperatorNotDefinedException(String detailMessage) {
        super(detailMessage);
    }
}
