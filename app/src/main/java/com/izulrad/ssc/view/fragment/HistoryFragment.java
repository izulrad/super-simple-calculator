package com.izulrad.ssc.view.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.izulrad.ssc.R;
import com.izulrad.ssc.util.DialogUtils;
import com.izulrad.ssc.util.DisplayUtils;
import com.izulrad.ssc.util.HistoryUtils;
import com.izulrad.ssc.util.Log;
import com.izulrad.ssc.util.SystemUtils;
import com.izulrad.ssc.view.dialog.constant.AddConstantDialog;

import java.util.Map;

/**
 * Fragment contain a list of history calculation
 */
public class HistoryFragment extends Fragment {
    public static final String TAG = HistoryFragment.class.getName();

    public static Fragment newInstance() {
        Log.d(TAG, "newInstance ");
        return new HistoryFragment();
    }

    private HistoryListAdapter mHistoryListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView ");
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        ListView historyList = (ListView) view.findViewById(R.id.lv_history);
        mHistoryListAdapter = new HistoryListAdapter(getActivity().getApplicationContext());
        historyList.setAdapter(mHistoryListAdapter);
        historyList.setOnItemClickListener(new HistoryListOnItemClickListener());
        registerForContextMenu(historyList);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.d(TAG, "onCreateOptionsMenu ");
        menu.clear();
        menu.add(Menu.NONE, 0, 0, R.string.history_clear);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected ");
        switch (item.getItemId()) {
            case 0:
                DialogUtils.showSimpleAttentionAlertDialog(getActivity(),
                        getString(R.string.dialog_history_clear_history_list),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                HistoryUtils.clear();
                                mHistoryListAdapter.notifyDataSetChanged();
                            }
                        }
                );
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        Log.d(TAG, "onCreateContextMenu ");
        menu.add(Menu.NONE, 0, 0, R.string.menu_item_history_expression_to_display);
        menu.add(Menu.NONE, 1, 0, R.string.menu_item_history_copy_result);
        menu.add(Menu.NONE, 2, 0, R.string.menu_item_history_copy_all_expression);
        menu.add(Menu.NONE, 3, 0, R.string.menu_item_history_add_result_as_constant);
        menu.add(Menu.NONE, 4, 0, R.string.menu_item_history_delete);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Log.d(TAG, "onContextItemSelected " + item);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        TextView tvExpression = (TextView) info.targetView.findViewById(R.id.list_item_history_expression);

        final String label,
                expression = tvExpression.getText().toString(),
                result = HistoryUtils.getResult(expression);

        switch (item.getItemId()) {
            case 0:
                DisplayUtils.setTextAndMoveCursorToEndPosition(expression);
                AllControlsFragment.replace(R.id.container_controls, getFragmentManager());
                return true;
            case 1:
                label = getString(R.string.history_result_clip);
                SystemUtils.clipText(getActivity(), label, result);
                return true;
            case 2:
                label = getString(R.string.history_expression_clip);
                SystemUtils.clipText(getActivity(), label, expression + " = " + result);
                return true;
            case 3:
                AddConstantDialog.newInstance(result).show(getFragmentManager(), AddConstantDialog.TAG);
                return true;
            case 4:
                DialogUtils.showSimpleAttentionAlertDialog(getActivity(),
                        getString(R.string.dialog_history_delete_history_item),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                HistoryUtils.remove(expression);
                                mHistoryListAdapter.notifyDataSetChanged();
                            }
                        });
                return true;
            default:
                return false;
        }
    }

    private class HistoryListAdapter extends ArrayAdapter<Map.Entry<String, String>> {
        private LayoutInflater mInflater;
        private static final int mResource = R.layout.list_item_history;

        public HistoryListAdapter(Context context) {
            super(context, mResource);
            mInflater = getActivity().getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = mInflater.inflate(mResource, parent, false);
            } else {
                view = convertView;
            }

            Map.Entry<String, String> item = HistoryUtils.get(position);
            TextView tvExpression = (TextView) view.findViewById(R.id.list_item_history_expression);
            TextView tvResult = (TextView) view.findViewById(R.id.list_item_history_result);
            tvExpression.setText(item.getKey());
            tvResult.setText(item.getValue());

            return view;
        }

        @Override
        public int getCount() {
            return HistoryUtils.getSize();
        }
    }


    private class HistoryListOnItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Map.Entry<String, String> item = HistoryUtils.get(position);
            Log.d(TAG, "onItemClick " + item);
            DisplayUtils.appendTextOnCursorPosition(item.getValue());
            AllControlsFragment.replace(R.id.container_controls, getFragmentManager());
        }
    }
}
