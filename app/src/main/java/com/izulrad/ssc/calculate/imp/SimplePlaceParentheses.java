package com.izulrad.ssc.calculate.imp;

import com.izulrad.ssc.calculate.CalculateUtils;
import com.izulrad.ssc.calculate.PlaceParentheses;
import com.izulrad.ssc.calculate.exception.WrongParentheses;
import com.izulrad.ssc.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimplePlaceParentheses implements PlaceParentheses {
    private static final String TAG = SimplePlaceParentheses.class.getName();

    private final String incompleteMultiplication = "((?<=[\\d.)])\\()";

    private final Pattern negativeNumbersPattern =
            Pattern.compile("(?<![\\d.]|[\\w&&[\\D]])((-[\\d.]+)|((?<!\\(|\\))(-\\()*-[\\d.]+\\)*))(?![\\^\\)\\d.])");

    private final Pattern wrongWrap =
//      Pattern.compile("(\\([\\d.]+\\))");
            Pattern.compile("(?<![\\w&&[\\D]])(\\([\\d.]+\\))");

    private final Pattern incompleteFunction = Pattern.compile("(?<=[\\w&&[\\D]])(-?[\\d.]+)");

    private final Pattern exponentPattern = Pattern.compile("\\^");

    private final Pattern multiplicationAndDivisionPattern = Pattern.compile("(\\*|/)");

    private final Pattern negativeExpression =
            Pattern.compile("(?<=^|[^\\d.)(])-(?=(\\(-?(\\d|[\\w&&[\\D]])))");


    @Override
    public String place(String expression) {
        Log.d(TAG, "place before " + expression);
        String fixed = expression
                .replaceAll(incompleteMultiplication, "*(");

        StringBuilder result = new StringBuilder(fixed);

        openWrongWrap(result, wrongWrap);
        placeForPattern(result, negativeNumbersPattern);
        placeForPattern(result, incompleteFunction);
        findAndPlaceForPattern(result, multiplicationAndDivisionPattern);
        findAndPlaceForPattern(result, exponentPattern);
        findAndPlaceForPattern(result, negativeExpression);

        if (isAlreadyHaveParentheses(result.toString(), 0, result.length() - 1)) {
            result.deleteCharAt(result.length() - 1);
            result.deleteCharAt(0);
        }

        Log.d(TAG, "place after " + result.toString());
        return result.toString();
    }

    private boolean isAlreadyHaveParentheses(String expression, int start, int end) {
        if (start < 0 && end >= expression.length()) return true;

        int startIndex = start < 0 ? 0 : start;
        int endIndex = end >= expression.length() ? expression.length() - 1 : end;

        int startChar = expression.charAt(startIndex);
        if (startChar != '(') return false;

        int endChar = expression.charAt(endIndex);
        if (endChar != ')') return false;

        return CalculateUtils.findPairForParenthesis(expression, startIndex) == endIndex;
    }

    /* Open if not negative number wrapped in parentheses*/
    private void openWrongWrap(StringBuilder sb, Pattern pattern) {
        Matcher matcher = pattern.matcher(sb);
        while (matcher.find()) {
            sb.replace(matcher.start(), matcher.end(), CalculateUtils.openParentheses(matcher.group()));
            matcher.reset(sb);
        }
    }

    /**
     * Place parentheses for pattern
     */
    private void placeForPattern(StringBuilder sb, Pattern pattern) {
        Matcher matcher = pattern.matcher(sb);
        while (matcher.find()) {
            if (matcher.start() == 0 && matcher.end() == sb.length()) break;
            insertParentheses(sb, matcher.start(), matcher.end());
            matcher.reset(sb);
        }
    }

    /**
     * Find index open and close parentheses, start with pattern.
     * And place parentheses to found indexes
     */
    private void findAndPlaceForPattern(StringBuilder sb, Pattern pattern) {
        Matcher matcher = pattern.matcher(sb);

        char[] allowedChars;
        int location = 0;
        while (matcher.find(location)) {
            location = matcher.start();

            if (matcher.group().equals("*")) allowedChars = new char[]{'*'};
            else allowedChars = null;


            int start = findStartIndex(sb, location, allowedChars);
            int end = findEndIndex(sb, location, allowedChars);

            if (start == -2 || end == -2) throw new WrongParentheses();

            if (isAlreadyHaveParentheses(sb.toString(), start, end)) {
                location++;
            } else {
                insertParentheses(sb, start + 1, end);
                location += 2;
                matcher.reset(sb);
            }
        }
    }

    private int findStartIndex(StringBuilder sb, int start, char... allowedChars) {
        int count = 0;

        for (int i = start - 1; i >= 0; i--) {
            char c = sb.charAt(i);
            if (c == ')') count++;
            boolean allowed = Character.isDigit(c) || c == '.'
                    || (allowedChars != null && arrayContain(allowedChars, c));
            if (!allowed && count == 0) return i;
            if (c == '(') count--;
        }

        return count == 0 ? -1 : -2;
    }

    private int findEndIndex(StringBuilder sb, int start, char... allowedChars) {
        int count = 0;

        for (int i = start + 1; i < sb.length(); i++) {
            char c = sb.charAt(i);
            if (c == '(') count++;
            boolean allowed = Character.isDigit(c) || c == '.'
                    || (allowedChars != null && arrayContain(allowedChars, c));
            if (!allowed && count == 0) return i;
            if (c == ')') count--;
        }

        return count == 0 ? sb.length() : -2;
    }

    private boolean arrayContain(char[] arr, char c) {
        for (char cc : arr) if (cc == c) return true;
        return false;
    }

    private void insertParentheses(StringBuilder stringBuilder, int start, int end) {
        Log.d(TAG, String.format("insertParentheses %s start %d end %d", stringBuilder.toString(), start, end));
        stringBuilder.insert(end, ')');
        stringBuilder.insert(start, '(');
    }
}
