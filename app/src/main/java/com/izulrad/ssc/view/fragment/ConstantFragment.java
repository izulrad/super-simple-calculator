package com.izulrad.ssc.view.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.izulrad.ssc.R;
import com.izulrad.ssc.util.ConstantUtils;
import com.izulrad.ssc.util.DialogUtils;
import com.izulrad.ssc.util.DisplayUtils;
import com.izulrad.ssc.util.Log;
import com.izulrad.ssc.util.SystemUtils;
import com.izulrad.ssc.view.dialog.constant.AddConstantDialog;
import com.izulrad.ssc.view.dialog.constant.EditConstantDialog;

import java.util.Map;

/**
 * Fragment contain a list of constant
 */
public class ConstantFragment extends Fragment {
    public static final String TAG = ControlFragment.class.getName();
    private static final int REQUEST_CODE = 1;

    public static Fragment newInstance() {
        Log.d(TAG, "newInstance ");
        return new ConstantFragment();
    }

    private ConstantListAdapter mConstantListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView ");
        View view = inflater.inflate(R.layout.fragment_constant, container, false);
        ListView constantsList = (ListView) view.findViewById(R.id.lv_constants);
        mConstantListAdapter = new ConstantListAdapter(getActivity());
        constantsList.setAdapter(mConstantListAdapter);
        constantsList.setOnItemClickListener(new ConstantOnItemClickListener());
        registerForContextMenu(constantsList);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.d(TAG, "onCreateOptionsMenu ");
        menu.clear();
        menu.add(Menu.NONE, 0, 0, R.string.menu_item_constants_add_new);
        menu.add(Menu.NONE, 1, 0, R.string.menu_item_constants_restore_default);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected ");
        switch (item.getItemId()) {
            case 0:
                DialogFragment df = AddConstantDialog.newInstance(null);
                df.setTargetFragment(this, REQUEST_CODE);
                df.show(getFragmentManager(), AddConstantDialog.TAG);
                return true;
            case 1:
                DialogUtils.showSimpleAttentionAlertDialog(getActivity(),
                        getString(R.string.dialog_constant_restore_to_default),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ConstantUtils.restoreDefault(getActivity());
                                mConstantListAdapter.notifyDataSetChanged();
                            }
                        });
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        Log.d(TAG, "onCreateContextMenu ");
        menu.add(Menu.NONE, 0, 0, R.string.menu_item_constants_edit_value);
        menu.add(Menu.NONE, 1, 0, R.string.menu_item_constants_edit_name);
        menu.add(Menu.NONE, 2, 0, R.string.menu_item_constants_copy_value);
        menu.add(Menu.NONE, 3, 0, R.string.menu_item_constants_copy_name_and_value);
        menu.add(Menu.NONE, 4, 0, R.string.menu_item_constants_delete);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Log.d(TAG, "onContextItemSelected " + item);
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        TextView tvName = (TextView) info.targetView.findViewById(R.id.list_item_constant_name);

        final String label,
                name = tvName.getText().toString(),
                value = ConstantUtils.getValue(name);

        switch (item.getItemId()) {
            case 0:
                showEditConstantDialog(name, EditConstantDialog.EDIT_VALUE_ACTION);
                return true;
            case 1:
                showEditConstantDialog(name, EditConstantDialog.EDIT_NAME_ACTION);
                return true;
            case 2:
                label = getString(R.string.constant_value);
                SystemUtils.clipText(getActivity(), label, value);
                return true;
            case 3:
                label = getString(R.string.constant_name_and_value);
                SystemUtils.clipText(getActivity(), label, name + " = " + value);
                return true;
            case 4:
                DialogUtils.showSimpleAttentionAlertDialog(getActivity(),
                        getString(R.string.dialog_constant_delete_constant),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ConstantUtils.delete(name);
                                mConstantListAdapter.notifyDataSetChanged();
                            }
                        });
                return true;
            default:
                return false;
        }
    }

    private void showEditConstantDialog(String name, int action) {
        DialogFragment df = EditConstantDialog.newInstance(name, action);
        df.setTargetFragment(this, REQUEST_CODE);
        df.show(getFragmentManager(), EditConstantDialog.TAG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult ");
        if (requestCode == REQUEST_CODE) {
            mConstantListAdapter.notifyDataSetChanged();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private class ConstantListAdapter extends ArrayAdapter<Map.Entry<String, String>> {
        private static final int mResource = R.layout.list_item_constant;
        private LayoutInflater mInflater;

        public ConstantListAdapter(Context context) {
            super(context, mResource);
            mInflater = getActivity().getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = mInflater.inflate(mResource, parent, false);
            } else {
                view = convertView;
            }

            Map.Entry<String, String> constant = ConstantUtils.get(position);
            TextView tvName = (TextView) view.findViewById(R.id.list_item_constant_name);
            TextView tvValue = (TextView) view.findViewById(R.id.list_item_constant_constant_value);
            tvName.setText(constant.getKey());
            tvValue.setText(constant.getValue());

            return view;
        }

        @Override
        public int getCount() {
            return ConstantUtils.getSize();
        }
    }

    private class ConstantOnItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d(TAG, "onItemClick ");
            Map.Entry<String, String> constant = ConstantUtils.get(position);
            DisplayUtils.appendTextOnCursorPosition(constant.getValue());
            AllControlsFragment.replace(R.id.container_controls, getFragmentManager());
        }
    }
}
