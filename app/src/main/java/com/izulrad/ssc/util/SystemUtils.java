package com.izulrad.ssc.util;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.widget.Toast;

public class SystemUtils {
    private static final String TAG = SystemUtils.class.getName();

    public static void clipText(Context context, String label, String text) {
        Log.d(TAG, "clipText " + label + " " + text);
        ClipData clip = ClipData.newPlainText(label, text);
        ClipboardManager clipboard = (ClipboardManager)
                context.getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setPrimaryClip(clip);

        Toast.makeText(context, label + " copy", Toast.LENGTH_SHORT).show();
    }
}
