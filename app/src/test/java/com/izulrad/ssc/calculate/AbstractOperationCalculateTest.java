package com.izulrad.ssc.calculate;


import com.izulrad.ssc.util.Log;
import com.izulrad.ssc.util.UserSetting;

import static com.izulrad.ssc.calculate.CalculateUtils.openParentheses;
import static org.junit.Assert.assertEquals;

abstract public class AbstractOperationCalculateTest {
  protected OperationCalculate operationCalculate;
  protected Double delta;

  /**
   * Sequence the testing data: leftOperand[0] operator[1] rightOperand[2] =[3] expected[4]
   * Split of " "
   */
  String[] operations = {
      "2 ^ 2 = 4",
      "2 / 2 = 1",
      "2 * 2 = 4",
      "2 + 2 = 4",
      "2 - 2 = 0",
      "(-2) - 2 = -4",
  };

  @org.junit.Test
  public void testOperation() throws Exception {
    if (operationCalculate == null)
      throw new NullPointerException("operationCalculate == null, use setUp method");
    if (delta == null) throw new NullPointerException("delta == null, use setUp method");
    UserSetting.loadDefault();
    Log.setDebugMode(false);

    for (String s : operations) {
      String[] op = s.split(" ");
      String leftOperand = op[0];
      String operator = op[1];
      String rightOperand = op[2];
      Double expected = Double.valueOf(op[4]);
      Double actual = Double.valueOf(openParentheses(operationCalculate.operation(
          leftOperand, rightOperand, operator)));

      assertEquals(String.format("operation %s %s %s = ?", leftOperand, operator, rightOperand),
          expected, actual, delta);
    }
  }
}
