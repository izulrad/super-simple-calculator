package com.izulrad.ssc.calculate;

import java.util.List;

public interface ExpressionCheck {
    List<Segment> check(String expression);
}
