package com.izulrad.ssc.calculate.imp;

import com.izulrad.ssc.calculate.CalculateUtils;
import com.izulrad.ssc.calculate.Function;
import com.izulrad.ssc.calculate.FunctionCalculate;
import com.izulrad.ssc.calculate.exception.FunctionNotDefinedException;
import com.izulrad.ssc.util.Log;
import com.izulrad.ssc.util.UserSetting;

import java.util.HashMap;
import java.util.Map;


public class SimpleFunctionCalculate implements FunctionCalculate {
    private static final String TAG = SimpleFunctionCalculate.class.getName();

    private static final Map<Function, ActionHandler> actions =
            new HashMap<Function, ActionHandler>() {{
                put(Function.ABS, new ActionHandler() {
                    @Override
                    public Double run(Double x) {
                        return Math.abs(x);
                    }
                });
                put(Function.LG, new ActionHandler() {
                    @Override
                    public Double run(Double x) {
                        return Math.log10(x);
                    }
                });
                put(Function.LN, new ActionHandler() {
                    @Override
                    public Double run(Double x) {
                        return Math.log(x);
                    }
                });
                put(Function.SIN, new ActionHandler() {
                    @Override
                    public Double run(Double x) {
                        return Math.sin(x);
                    }
                });
                put(Function.COS, new ActionHandler() {
                    @Override
                    public Double run(Double x) {
                        return Math.cos(x);
                    }
                });
                put(Function.TAN, new ActionHandler() {
                    @Override
                    public Double run(Double x) {
                        return Math.tan(x);
                    }
                });
                put(Function.CTAN, new ActionHandler() {
                    @Override
                    public Double run(Double x) {
                        return 1 / Math.tan(x);
                    }
                });
                put(Function.SEC, new ActionHandler() {
                    @Override
                    public Double run(Double x) {
                        return 1 / Math.cos(x);
                    }
                });
                put(Function.COSEC, new ActionHandler() {
                    @Override
                    public Double run(Double x) {
                        return 1 / Math.sin(x);
                    }
                });
                put(Function.ASIN, new ActionHandler() {
                    @Override
                    public Double run(Double x) {
                        return Math.asin(x);
                    }
                });
                put(Function.ACOS, new ActionHandler() {
                    @Override
                    public Double run(Double x) {
                        return Math.acos(x);
                    }
                });
                put(Function.ATAN, new ActionHandler() {
                    @Override
                    public Double run(Double x) {
                        return Math.atan(x);
                    }
                });
                put(Function.SINH, new ActionHandler() {
                    @Override
                    public Double run(Double x) {
                        return Math.sinh(x);
                    }
                });
                put(Function.COSH, new ActionHandler() {
                    @Override
                    public Double run(Double x) {
                        return Math.cosh(x);
                    }
                });
                put(Function.TANH, new ActionHandler() {
                    @Override
                    public Double run(Double x) {
                        return Math.tanh(x);
                    }
                });

            }};

    public String function(String function, String arg) {
        Log.d(TAG, String.format("function %s(%s)", function, arg));
        double result,
                x = Double.parseDouble(CalculateUtils.openParentheses(arg));

        Function f = Function.getFunctionForSign(function);

        if (f.isTrigonometric()) {
            switch (UserSetting.getCurrentTrigonometricMeasure()) {
                case CalculateUtils.DEGREES:
                    x = Math.toRadians(x);
                    break;
                case CalculateUtils.GRADES:
                    x *= CalculateUtils.RADIANS_IN_GRADE;
                    break;
                case CalculateUtils.MINUTES:
                    x *= CalculateUtils.RADIANS_IN_MINUTE;
                    break;
                case CalculateUtils.SECONDS:
                    x *= CalculateUtils.RADIANS_IN_SECOND;
                    break;
                case CalculateUtils.RADIANS:
                default:
                    //do nothing
            }
        }

        ActionHandler action = actions.get(f);
        if (action == null) {
            throw new FunctionNotDefinedException(String.format("function %s is not defined", function));
        }
        result = action.run(x);
        return CalculateUtils.doubleToString(result);
    }

    private interface ActionHandler {
        Double run(Double x);
    }
}
