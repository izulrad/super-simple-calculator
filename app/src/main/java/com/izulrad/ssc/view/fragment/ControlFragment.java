package com.izulrad.ssc.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.izulrad.ssc.util.Log;

/**
 * Fragment of control panel, used a different layout, them names start with fragment_control_{number}
 */
public class ControlFragment extends Fragment {
    private static final String TAG = ControlFragment.class.getName();
    private static final String LAYOUT_ID = "layoutId";

    public static Fragment newInstance(int layoutId) {
        Log.d(TAG, "newInstance");
        Fragment result = new ControlFragment();

        Bundle args = new Bundle();
        args.putInt(LAYOUT_ID, layoutId);
        result.setArguments(args);

        return result;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView ");
        View view = inflater.inflate(getArguments().getInt(LAYOUT_ID), container, false);

        return view;
    }
}
