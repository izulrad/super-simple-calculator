package com.izulrad.ssc.calculate;

import com.izulrad.ssc.util.Log;
import com.izulrad.ssc.util.UserSetting;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.RoundingMode;

import static junit.framework.Assert.assertEquals;

public class CalculateUtilsTest {
  @Before
  public void setUp() throws Exception {
    UserSetting.loadDefault();
  }


  /**
   * Sequence the testing data: expression[0] expected[1]
   * Split of "="
   */
  @Test
  public void testOpenParentheses() throws Exception {
    Log.setDebugMode(false);

    String[] strings = {
        "(1+1)=1+1",
        "(2.123+321.23)=2.123+321.23",
        "(412+(-23)+22)=412+(-23)+22",
        "((2+1)-2)=(2+1)-2"
    };

    for (String s : strings) {
      String[] arr = s.split("=");
      String expression = arr[0];
      String expected = arr[1];
      String actual = CalculateUtils.openParentheses(expression);

      assertEquals("it's should be equals", expected, actual);
    }
  }

  /**
   * Sequence the testing data: expression[0] expected[1]
   * Split of "="
   */
  @Test
  public void testDoubleToString() throws Exception {
    Log.setDebugMode(false);

    //DEFAULT SETTING
    UserSetting.loadDefault();
    checkConvertDoubleToString(new String[]{
        "-0.001=(-0.001)",
        "-1.2=(-1.2)",
        "-0.0000000000000000012=0",
        "-0.0000012=(-0.0000012)",
        "1.2E-10=0",
        "-1.2E-7=(-0.00000012)",
        "0.000000005=0.00000001",
        "1.9E-8=0.00000002",
    });

    //SET SCALE 19
    UserSetting.loadDefault();
    UserSetting.setCurrentNumberScale(19);
    checkConvertDoubleToString(new String[]{
        "0.00000000000000000125=0.0000000000000000013",
        "1.2E-10=0.00000000012",
    });

    //SET ROUNDING MODE RoundingMode.DOWN
    UserSetting.loadDefault();
    UserSetting.setCurrentRoundingMode(RoundingMode.DOWN);
    checkConvertDoubleToString(new String[]{
        "0.000000005=0",
        "1.9E-8=0.00000001",
    });
  }

  private void checkConvertDoubleToString(String[] data) {
    for (String s : data) {
      String[] arr = s.split("=");
      String expected = arr[1];
      String actual = CalculateUtils.doubleToString(Double.valueOf(arr[0]));
      Assert.assertEquals("it's should be equals", expected, actual);
    }
  }

  /**
   * Sequence the testing data: data[0](expression[0] index[1] split  of " ") expected[1]
   * Split of "="
   */
  @Test
  public void testFindPairForParenthesis() throws Exception {
    Log.setDebugMode(false);

    String[] strings = {
        "0+(3+5)+8 2=6",
        "0+(3+5)+8 6=2",
        "(1+3+(6+8)+1) 0=12",
        "(1+3+(6+8)+1) 12=0",
        "0+((4+6)+9) 2=10",
        "0+((4+6)+9) 10=2",
        "(() 2=1",
        "()) 2=-1",
        "(() 1=2",
        "(() 0=-1"
    };

    for (String s : strings) {
      String[] arr = s.split("=");
      String[] data = arr[0].split(" ");
      String expression = data[0];
      int index = Integer.parseInt(data[1]);
      int expected = Integer.parseInt(arr[1]);
      int actual = CalculateUtils.findPairForParenthesis(expression, index);

      assertEquals("it's should be equals", expected, actual);
    }

  }
}