package com.izulrad.ssc.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.izulrad.ssc.R;
import com.izulrad.ssc.util.DisplayUtils;
import com.izulrad.ssc.util.Log;

/**
 * Fragment include all controls fragment and a calculate display
 */
public class CalculateFragment extends Fragment {
    public static final String TAG = CalculateFragment.class.getName();

    public static Fragment newInstance() {
        Log.d(TAG, "newInstance");
        return new CalculateFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.fragment_calc, container, false);

        DisplayUtils.setDisplay((EditText) view.findViewById(R.id.tv_display));

        getFragmentManager().beginTransaction()
                .replace(R.id.container_controls, AllControlsFragment.newInstance(), AllControlsFragment.TAG)
                .commit();

        return view;
    }
}
