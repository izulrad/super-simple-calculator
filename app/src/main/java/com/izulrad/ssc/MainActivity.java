package com.izulrad.ssc;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.izulrad.ssc.util.ControlFragmentUtils;
import com.izulrad.ssc.util.HistoryUtils;
import com.izulrad.ssc.util.Log;
import com.izulrad.ssc.util.UserSetting;
import com.izulrad.ssc.view.fragment.AllControlsFragment;
import com.izulrad.ssc.view.fragment.CalculateFragment;
import com.izulrad.ssc.view.fragment.ConstantFragment;
import com.izulrad.ssc.view.fragment.HistoryFragment;
import com.izulrad.ssc.view.fragment.SettingFragment;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getName();
    private static final int BACK_PRESS_INTERVAL = 3000;

    private long backPressTime;

    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        UserSetting.load(getApplicationContext());
        ControlFragmentUtils.initStringResources(getApplicationContext());

        setContentView(R.layout.layout_main);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_container, CalculateFragment.newInstance(),
                        CalculateFragment.TAG)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu");
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected");
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.main_container, SettingFragment.newInstance())
                        .commit();
                return true;
            case R.id.action_history:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container_controls, HistoryFragment.newInstance(),
                                HistoryFragment.TAG)
                        .commit();
                return true;
            case R.id.action_constant:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container_controls, ConstantFragment.newInstance(),
                                ConstantFragment.TAG)
                        .commit();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        UserSetting.save(getApplicationContext());
        HistoryUtils.clear();
        super.onDestroy();

    }

    @Override
    public void onBackPressed() {
        Fragment calcFragment = getSupportFragmentManager()
                .findFragmentByTag(CalculateFragment.TAG);

        if (calcFragment == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_container, CalculateFragment.newInstance(),
                            CalculateFragment.TAG)
                    .commit();
            return;
        }


        Fragment allControls = getSupportFragmentManager()
                .findFragmentByTag(AllControlsFragment.TAG);

        if (allControls != null) {

            if (backPressTime + BACK_PRESS_INTERVAL > System.currentTimeMillis()) {
                super.onBackPressed();
            } else {
                backPressTime = System.currentTimeMillis();
                Toast.makeText(getApplicationContext(), R.string.toast_press_back_to_exit,
                        Toast.LENGTH_SHORT).show();
            }

        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_controls, AllControlsFragment.newInstance(),
                            AllControlsFragment.TAG)
                    .commit();
        }
    }

    public void onButtonClick(View view) {
        Log.d(TAG, "onButtonClick");
        ControlFragmentUtils.click(((Button) view).getText().toString());
    }
}
