package com.izulrad.ssc.calculate.imp;

import com.izulrad.ssc.calculate.CalculateUtils;
import com.izulrad.ssc.calculate.ExpressionCheck;
import com.izulrad.ssc.calculate.Function;
import com.izulrad.ssc.calculate.Segment;
import com.izulrad.ssc.util.Log;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Check string expression for wrong sequence character
 */
public class SimpleExpressionCheck implements ExpressionCheck {
    private static final String TAG = SimpleExpressionCheck.class.getName();

    private final Pattern wrongNumberPattern = Pattern.compile("((?<=^|[^\\d.])\\.+(?=[^\\d.]|$))");
    private final Pattern multipointPattern = Pattern.compile("([\\d.]*\\.[\\d.]*\\.[\\d.]*)");
    private final Pattern functionPattern = Pattern.compile("([\\w&&[\\D]]+)");
    private final Pattern wrongOperationSequencePattern =
            Pattern.compile("(?<=[\\w&&[\\D]])([+/*^])|(^[+/*^]+)|([-+/*^]+[+/*^]+)|([-+/*^]+)$");
    private final Pattern wrongParenthesesSequencePattern =
            Pattern.compile("((?<=\\()([+*/^]{1}))|(\\)[\\d.]{1})");

    @Override
    public List<Segment> check(String expression) {
        Log.d(TAG, "check " + expression);
        List<Segment> segments = new LinkedList<>();

        segments.addAll(getSegment(expression, multipointPattern));
        segments.addAll(getSegment(expression, wrongNumberPattern));
        segments.addAll(getFunctionSegment(expression, functionPattern));
        segments.addAll(getSegment(expression, wrongOperationSequencePattern));
        segments.addAll(getSegment(expression, wrongParenthesesSequencePattern));
        segments.addAll(getWrongParentheses(expression));

        return segments;
    }

    private List<Segment> getSegment(String expression, Pattern pattern) {
        List<Segment> result = new LinkedList<>();
        Matcher matcher = pattern.matcher(expression);
        while (matcher.find()) {
            result.add(new Segment(matcher.start(), matcher.end()));
        }
        return result;
    }

    private List<Segment> getFunctionSegment(String expression, Pattern pattern) {
        List<Segment> result = new LinkedList<>();
        Matcher matcher = pattern.matcher(expression);
        while (matcher.find()) {
            if (!Function.hasFunction(matcher.group())) {
                result.add(new Segment(matcher.start(), matcher.end()));
            }
        }
        return result;
    }

    private List<Segment> getWrongParentheses(String expression) {
        List<Segment> result = new LinkedList<>();
        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) == '(' || expression.charAt(i) == ')') {
                int end = CalculateUtils.findPairForParenthesis(expression, i);
                if (end == -1) result.add(new Segment(i, i + 1));
                else if (i + 1 == end) result.add(new Segment(i, end + 1));
            }
        }
        return result;
    }
}
