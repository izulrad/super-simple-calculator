package com.izulrad.ssc.calculate.imp;

import com.izulrad.ssc.calculate.CalculateUtils;
import com.izulrad.ssc.calculate.FunctionCalculate;
import com.izulrad.ssc.calculate.OperationCalculate;
import com.izulrad.ssc.calculate.ExpressionCalculate;
import com.izulrad.ssc.calculate.exception.OperandNotDefinedException;
import com.izulrad.ssc.calculate.exception.OperatorNotDefinedException;
import com.izulrad.ssc.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RecursiveExpressionCalculate implements ExpressionCalculate {
    private static final String TAG = RecursiveExpressionCalculate.class.getName();

    /**
     * Find parentheses in expression, but not negative number wrapped in parentheses
     * Find (1+1) or ((-1)+1) or ((-2)+(-2)) or ((-2)+1-(-2))
     * Not find (-1)
     */
    private final static Pattern PARENTHESES_PATTERN =
            Pattern.compile("\\((([^)(]+)|(\\(-?[\\d.]+\\))+)+\\)");

    /**
     * Find double negative numbers, like -(-2) or (-(-2))
     */
    private final static Pattern DOUBLE_NEGATIVE_PATTERN =
            Pattern.compile("^\\(?-\\(-[\\d.]+\\)\\)?$");

    /**
     * Find operation exponent
     */
    private final static Pattern EXPONENT_PATTERN = Pattern.compile("\\^");

    /**
     * Find operation multiplication or division
     */
    private final static Pattern MULTIPLICATION_DIVISION_PATTERN = Pattern.compile("(\\*)|(/)");

    /**
     * Find operation addition or subtraction
     */
    private final static Pattern ADDITION_SUBTRACTION_PATTERN = Pattern.compile("(\\+)|(?<=[\\d.\\)])-");

    /**
     * A number, like -2.0 (-2.0) 2.0
     */
    private static final String A_NUMBER =
            "((\\(-?[\\d.]+\\))|(-?[\\d.]+))";

    /**
     * A number, like (-2.0) 2.0
     */
    private static final String A_OPERATOR =
            "((\\(-[\\d.]+\\))|([\\d.]+))";

    private final FunctionCalculate mFunctionCalculate;
    private final OperationCalculate mOperationCalculate;

    public RecursiveExpressionCalculate(OperationCalculate operationCalculate,
                                        FunctionCalculate functionCalculate) {
        this.mOperationCalculate = operationCalculate;
        this.mFunctionCalculate = functionCalculate;
    }

    public String calculate(String expression) {
        Log.d(TAG, "calculate expression " + expression);
        try {
            String fixed =
                    Pattern.compile("((?<=[\\d.)])(?=[\\w&&[\\D]]))").matcher(expression).replaceAll("*");

            String result = openParenthesesAndCalc(fixed, 0);
            return CalculateUtils.openParentheses(result);
        } catch (NvmException ie) {
            return ie.getMessage();
        }
    }

    private String openParenthesesAndCalc(String expression, int start) {
        Log.d(TAG, "openParenthesesAndCalc expression " + expression);

        Matcher parenthesesMatcher = PARENTHESES_PATTERN.matcher(expression);

        if (parenthesesMatcher.find(start)) {//HAVE PARENTHESES
            String parentheses = parenthesesMatcher.group();
            String stringInParentheses = CalculateUtils.openParentheses(parentheses);

            Pattern functionPattern = Pattern.compile("[\\w&&[\\D]]+(?=\\Q" + parentheses + "\\E)");
            Matcher functionMatcher = functionPattern.matcher(expression);

            String result;
            String replaced;

            if (functionMatcher.find()) {//HAVE FUNCTION ON THE LEFT OF PARENTHESES
                String function = functionMatcher.group();
                result = mFunctionCalculate.function(function, openParenthesesAndCalc(stringInParentheses, 0));
                replaced = function + parentheses;
            } else {
                if (Pattern.matches(A_NUMBER, stringInParentheses)) {
                    return openParenthesesAndCalc(expression, start + 1);
                } else if (DOUBLE_NEGATIVE_PATTERN.matcher(stringInParentheses).find()) {
                    result = calc(stringInParentheses);
                } else {
                    result = openParenthesesAndCalc(stringInParentheses, 0);
                }
                replaced = parentheses;
            }

            String newExpression = replaceItInTo(replaced, expression, result);
            return openParenthesesAndCalc(newExpression, 0);
        } else {
            return calc(expression);
        }


    }

    /**
     * Calculate simple expression, without expression in parentheses,
     * but with negative numbers, like (-2)
     * <p/>
     * Example: 2+(-2)-2; 3*3*5*(-1)
     */
    private String calc(String expression) {
        Log.d(TAG, "calc " + expression);

        String operator;

        if (Pattern.matches(A_NUMBER, expression)) {
            return expression;
        } else {
            Matcher doubleNegativeMatcher = DOUBLE_NEGATIVE_PATTERN.matcher(expression);
            if (doubleNegativeMatcher.find()) {
                Matcher numberMatcher = Pattern.compile("[\\d.]+").matcher(expression);
                if (numberMatcher.find())
                    return replaceItInTo(doubleNegativeMatcher.group(), expression, numberMatcher.group());
                else
                    throw new OperandNotDefinedException("expression " + expression + " have not number");
            } else {

                operator = findOperator(expression, EXPONENT_PATTERN);
                if (operator == null)
                    operator = findOperator(expression, MULTIPLICATION_DIVISION_PATTERN);
                if (operator == null)
                    operator = findOperator(expression, ADDITION_SUBTRACTION_PATTERN);

                if (operator == null) {
                    throw new OperatorNotDefinedException("Operator not defined in expression " + expression);
                }

                String leftOperand = findLeftOperandForOperator(expression, operator);
                String rightOperand = findRightOperandForOperator(expression, operator);
                String replaced = leftOperand + operator + rightOperand;

                String result = mOperationCalculate.operation(leftOperand, rightOperand, operator);

                if (result.contains("Infinity")) throw new NvmException(result);
                if (result.contains("NaN")) throw new NvmException(result);

                return calc(replaceItInTo(replaced, expression, result));
            }
        }
    }

    private String findOperator(String expression, Pattern pattern) {
        Matcher m = pattern.matcher(expression);
        if (m.find()) return m.group();
        else return null;
    }

    /**
     * Examples: expression = "2+(-2)" operator = "+" find -2
     */
    private String findRightOperandForOperator(String expression, String operator) {
        Log.d(TAG, "findRightOperandForOperator ");
        Pattern rightOperandPattern = Pattern.compile("(?<=[\\d.]\\)?\\Q" + operator + "\\E)" + A_OPERATOR);
        return findOperand(expression, operator, rightOperandPattern);
    }

    /**
     * Examples: expression = "2+(-2)" operator = "+" find 2
     */
    private String findLeftOperandForOperator(String expression, String operator) {
        Log.d(TAG, "findLeftOperandForOperator ");
        Pattern leftOperandPattern = Pattern.compile(A_OPERATOR + "(?=\\Q" + operator + "\\E.+)");
        return findOperand(expression, operator, leftOperandPattern);
    }

    private String findOperand(String expression, String operator, Pattern operandPattern) {
        String result;
        Matcher operandMatcher = operandPattern.matcher(expression);

        if (operandMatcher.find()) {
            result = operandMatcher.group();
        } else {
            throw new OperandNotDefinedException(
                    String.format("Operand for operator %s in %s is not defined", operator, expression));
        }

        Log.d(TAG, String.format("findOperand %s to operator %s in %s", result, operator, expression));
        return result;
    }

    private String replaceItInTo(String it, String in, String to) {
        String result = in.replaceFirst("\\Q" + it + "\\E", to);
        Log.d(TAG, "replaceItInTo it " + it + " in " + in + " to " + to + " result = " + result);
        return result;
    }

    private class NvmException extends RuntimeException {
        public NvmException(String detailMessage) {
            super(detailMessage);
        }
    }
}
