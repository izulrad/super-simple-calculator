package com.izulrad.ssc.calculate;

import com.izulrad.ssc.util.Log;
import com.izulrad.ssc.util.UserSetting;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

abstract public class AbstractExpressionCheckTest {

  protected ExpressionCheck expressionCheck;

  /**
   * Sequence the testing data: expression[0] delimiter("=") expectedString[1]
   * Sequence the expectedString: groupDelimiter(";)
   * Sequence the group: startIndex[0] delimiter(",) endIndex
   */
  String[] expressions = {
      ".+2=0,1",
      "0+2-.+6=4,5",
      ".1.+4=0,3",
      "..2+4..+8..1=0,3;4,7;8,12",
      "+(2+4)6=0,1;5,7",
      "sin2=",
      "2cos(2)=",
      "notFunction(2)=0,11",
      "3qwe2=1,4",
      "+1=0,1",
      "-1=",
      "0++3=1,3",
      "--2=",
      "0+-3-*6=4,6",
      "(+5+2)=1,2",
      "(1+3)5=4,6"
  };

  @Test
  public void testCheck() throws Exception {
    if (expressionCheck == null)
      throw new NullPointerException("expressionCheck == null, use setUp method");
    UserSetting.loadDefault();
    Log.setDebugMode(false);

    for (String data : expressions) {
      String[] dataArr = data.split("=");
      String expression = dataArr[0];
      List<Segment> expected = expressionCheck.check(expression);
      List<Segment> actual = new LinkedList<>();

      if (dataArr.length > 1) for (String group : dataArr[1].split(";")) {
        String[] groupArr = group.split(",");
        int startIndex = Integer.parseInt(groupArr[0]);
        int endIndex = Integer.parseInt(groupArr[1]);
        actual.add(new Segment(startIndex, endIndex));
      }

      assertEquals("length the lists should be equals, expression " + expression,
          expected.size(), actual.size());
      for (int i = 0; i < expected.size(); i++) {
        Segment expectedSegment = expected.get(i);
        Segment actualSegment = actual.get(i);
        assertEquals("segment should be equals, expression " + expression,
            expectedSegment, actualSegment);
      }
    }

  }
}
