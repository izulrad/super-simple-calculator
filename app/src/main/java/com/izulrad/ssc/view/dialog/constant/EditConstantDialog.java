package com.izulrad.ssc.view.dialog.constant;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.izulrad.ssc.R;
import com.izulrad.ssc.util.ConstantUtils;
import com.izulrad.ssc.util.Log;
import com.izulrad.ssc.util.ViewUtils;

public class EditConstantDialog extends DialogFragment {
    public static final String TAG = EditConstantDialog.class.getName();
    private static final String CONSTANT_NAME = "constantName";
    private static final String ACTION = "action";

    public static final int EDIT_NAME_ACTION = 1;
    public static final int EDIT_VALUE_ACTION = 2;

    public static DialogFragment newInstance(String constantName, int action) {
        DialogFragment result = new EditConstantDialog();

        Bundle args = new Bundle();
        args.putString(CONSTANT_NAME, constantName);
        args.putInt(ACTION, action);
        result.setArguments(args);

        return result;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.d(TAG, "onCreateDialog ");

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_constant_edit, null);
        final EditText editText = (EditText) view.findViewById(R.id.dialog_edit_constant_edit_text);

        final int action = getArguments().getInt(ACTION);
        final String constantName = getArguments().getString(CONSTANT_NAME);
        final String constantValue = ConstantUtils.getValue(constantName);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setView(view);
        switch (action) {
            case EDIT_NAME_ACTION:
                dialogBuilder.setTitle(getString(R.string.dialog_constant_edit_edit_name_for) + " "
                        + constantValue);
                ViewUtils.setTextAndSelectTextAndShowKeyboard(getActivity(), editText, constantName);
                break;
            case EDIT_VALUE_ACTION:
                dialogBuilder.setTitle(getString(R.string.dialog_constant_edit_edit_value_for) + " "
                        + constantName);
                ViewUtils.setTextAndSelectTextAndShowKeyboard(getActivity(), editText, constantValue);
                break;
        }
        dialogBuilder.setPositiveButton("OK", null);
        dialogBuilder.setNegativeButton("Cancel", null);

        final AlertDialog mDialog = dialogBuilder.create();

        mDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnOk = mDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String text = editText.getText().toString();

                        boolean verify = ConstantUtils.verify(getActivity(), editText,
                                action == EDIT_NAME_ACTION);
                        if (!verify) return;

                        switch (action) {
                            case EDIT_NAME_ACTION:
                                ConstantUtils.editName(constantName, text);
                                break;
                            case EDIT_VALUE_ACTION:
                                ConstantUtils.editValue(constantName, text);
                                break;
                        }

                        Fragment fragment = getTargetFragment();
                        if (fragment != null) {
                            fragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                        }

                        mDialog.dismiss();
                    }
                });
            }
        });

        return mDialog;
    }
}
