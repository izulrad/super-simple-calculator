package com.izulrad.ssc.calculate.imp;

import com.izulrad.ssc.calculate.AbstractFunctionCalculateTest;

import org.junit.Before;

public class SimpleFunctionCalculateTest extends AbstractFunctionCalculateTest {
  @Before
  public void setUp() throws Exception {
    functionCalculate = new SimpleFunctionCalculate();
    delta = 0.000001d;
  }
}
