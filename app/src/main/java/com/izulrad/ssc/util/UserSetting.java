package com.izulrad.ssc.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.izulrad.ssc.R;
import com.izulrad.ssc.calculate.CalculateUtils;

import java.math.RoundingMode;

public class UserSetting {
    //todo сделать UI
    //todo textSize buttons, textSize display
    private static final String TAG = UserSetting.class.getName();
    private static final String FILE_NAME = "UserSetting";

    private static final int DEFAULT_CONTROL_PANEL = 0;
    private static final int DEFAULT_TRIGONOMETRIC_MEASURE = CalculateUtils.RADIANS;
    private static final int DEFAULT_NUMBER_SCALE = 8;
    private static final int DEFAULT_HISTORY_SIZE = 50;
    private static final String DEFAULT_ROUNDING_MODE = RoundingMode.HALF_UP.toString();

    private static final String CURRENT_CONTROL_PANEL = "currentControlPage";
    private static final String CURRENT_TRIGONOMETRIC_MEASURE = "currentTrigonometricMeasure";
    private static final String CURRENT_NUMBER_SCALE = "currentNumberScale";
    private static final String CURRENT_HISTORY_SIZE = "currentHistorySize";
    private static final String CURRENT_ROUNDING_MODE = "currentRoundingMode";
    private static final String CONSTANTS = "constants";
    private static final String HISTORY = "history";

    private static int currentControlPage;
    private static int currentTrigonometricMeasure;
    private static int currentNumberScale;
    private static int currentHistorySize;
    private static RoundingMode currentRoundingMode;

    public static void save(Context context) {
        Log.d(TAG, "save");
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        Editor editor = sp.edit();

        editor.putInt(CURRENT_CONTROL_PANEL, currentControlPage);
        editor.putInt(CURRENT_TRIGONOMETRIC_MEASURE, currentTrigonometricMeasure);
        editor.putInt(CURRENT_NUMBER_SCALE, currentNumberScale);
        editor.putInt(CURRENT_HISTORY_SIZE, currentHistorySize);
        editor.putString(CURRENT_ROUNDING_MODE, currentRoundingMode.toString());
        editor.putString(CONSTANTS, ConstantUtils.concat(context));
        editor.putString(HISTORY, HistoryUtils.concat(context));

        editor.apply();
    }

    public static void load(Context context) {
        Log.d(TAG, "load");
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);

        currentControlPage = sp.getInt(CURRENT_CONTROL_PANEL, DEFAULT_CONTROL_PANEL);

        currentTrigonometricMeasure =
                sp.getInt(CURRENT_TRIGONOMETRIC_MEASURE, DEFAULT_TRIGONOMETRIC_MEASURE);

        currentNumberScale = sp.getInt(CURRENT_NUMBER_SCALE, DEFAULT_NUMBER_SCALE);

        currentHistorySize = sp.getInt(CURRENT_HISTORY_SIZE, DEFAULT_HISTORY_SIZE);

        currentRoundingMode =
                RoundingMode.valueOf(sp.getString(CURRENT_ROUNDING_MODE, DEFAULT_ROUNDING_MODE));

        ConstantUtils.init(context,
                sp.getString(CONSTANTS, context.getResources().getString(R.string.constants_default)));

        HistoryUtils.init(context, sp.getString(HISTORY, ""));
    }

    //Method for unit tests
    public static void loadDefault() {
        currentControlPage = DEFAULT_CONTROL_PANEL;
        currentTrigonometricMeasure = DEFAULT_TRIGONOMETRIC_MEASURE;
        currentNumberScale = DEFAULT_NUMBER_SCALE;
        currentHistorySize = DEFAULT_HISTORY_SIZE;
        currentRoundingMode = RoundingMode.valueOf(DEFAULT_ROUNDING_MODE);
    }

    public static void setCurrentControlPage(int currentControlPage) {
        UserSetting.currentControlPage = currentControlPage;
    }

    public static int getCurrentControlPage() {
        return currentControlPage;
    }

    public static int getCurrentTrigonometricMeasure() {
        return currentTrigonometricMeasure;
    }

    public static void setCurrentTrigonometricMeasure(int currentTrigonometricMeasure) {
        UserSetting.currentTrigonometricMeasure = currentTrigonometricMeasure;
    }

    public static int getCurrentNumberScale() {
        return currentNumberScale;
    }

    public static void setCurrentNumberScale(int currentNumberScale) {
        UserSetting.currentNumberScale = currentNumberScale;
    }

    public static RoundingMode getCurrentRoundingMode() {
        return currentRoundingMode;
    }

    public static void setCurrentRoundingMode(RoundingMode currentRoundingMode) {
        UserSetting.currentRoundingMode = currentRoundingMode;
    }

    public static int getCurrentHistorySize() {
        return currentHistorySize;
    }

    public static void setCurrentHistorySize(int currentHistorySize) {
        UserSetting.currentHistorySize = currentHistorySize;
    }
}
