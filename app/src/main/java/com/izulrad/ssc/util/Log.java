package com.izulrad.ssc.util;

public class Log {
    private static boolean debugMode = true;

    public static void e(String tag, String msg) {
        if (debugMode) {
            try {
                android.util.Log.e(tag, msg);
            } catch (RuntimeException re) {
                System.out.println(tag + " " + msg);
            }
        }
    }

    public static void d(String tag, String msg) {
        if (debugMode) {
            try {
                android.util.Log.d(tag, msg);
            } catch (RuntimeException re) {
                System.out.println(tag + " " + msg);
            }
        }
    }

    public static void setDebugMode(boolean debugMode) {
        Log.debugMode = debugMode;
    }
}