package com.izulrad.ssc.util;

import android.text.InputType;
import android.text.Spannable;
import android.widget.EditText;

public class DisplayUtils {
    private static final String TAG = DisplayUtils.class.getName();
    private static EditText display;

    public static void setDisplay(EditText display) {
        Log.d(TAG, "setDisplay");
        disableSoftInputFromEditText(display);
        DisplayUtils.display = display;
    }

    public static void appendTextOnCursorPosition(String text) {
        Log.d(TAG, "appendTextOnCursorPosition " + text);
        int start = display.getSelectionStart();
        int end = display.getSelectionEnd();
        display.getText().replace(start, end, text);
    }

    public static void setText(String text) {
        Log.d(TAG, "setText string " + text);
        display.setText(text);
    }

    public static void setText(Spannable text) {
        Log.d(TAG, "setText spannable " + text);
        display.setText(text);
    }

    public static void setTextAndMoveCursorToEndPosition(String text) {
        Log.d(TAG, "setTextAndMoveCursorToEndPosition " + text);
        DisplayUtils.setText(text);
        DisplayUtils.moveCursorToEndPosition();
    }

    public static String getText() {
        Log.d(TAG, "getText");
        return display.getText().toString();
    }

    public static void deleteCharacterBeforeCursorPosition() {
        Log.d(TAG, "deleteCharacterBeforeCursorPosition");
        int start = display.getSelectionStart();
        int end = display.getSelectionEnd();
        if (start == end && start > 0) display.getText().delete(start - 1, start);
        else display.getText().delete(start, end);
    }

    public static void clear() {
        Log.d(TAG, "clear");
        display.setText("");
    }

    public static void moveCursorToEndPosition() {
        Log.d(TAG, "moveCursorToEndPosition ");
        display.setSelection(display.getText().length());
    }

    /**
     * Disable soft keyboard from appearing,
     * use in conjunction with android:windowSoftInputMode="stateAlwaysHidden|adjustNothing"
     *
     * @param editText
     */
    private static void disableSoftInputFromEditText(EditText editText) {
        Log.d(TAG, "disableSoftInputFromEditText ");
        editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
        editText.setTextIsSelectable(true);
    }

    public static void moveCursorToLeft() {
        int selectionStart = display.getSelectionStart();
        if (--selectionStart >= 0) {
            display.clearFocus();
            display.setSelection(selectionStart);
        }
    }

    public static void moveCursorToRight() {
        int selectionEnd = display.getSelectionEnd();
        if (++selectionEnd <= display.getText().length()) {
            display.clearFocus();
            display.setSelection(selectionEnd);
        }
    }
}
