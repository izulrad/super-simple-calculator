package com.izulrad.ssc.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;

import com.izulrad.ssc.R;

public class DialogUtils {

    public static void showSimpleAttentionAlertDialog(Context context, String message,
                                                      OnClickListener onPositiveButtonClickListener) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.attention)
                .setMessage(message)
                .setPositiveButton(R.string.ok, onPositiveButtonClickListener)
                .setNegativeButton(R.string.cancel, null)
                .create()
                .show();
    }
}
