package com.izulrad.ssc.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.izulrad.ssc.R;
import com.izulrad.ssc.util.ControlFragmentUtils;
import com.izulrad.ssc.util.Log;
import com.izulrad.ssc.util.UserSetting;

/**
 * Fragment include all control panel, used a viewpager
 */
public class AllControlsFragment extends Fragment {
    public static final String TAG = AllControlsFragment.class.getName();

    public static Fragment newInstance() {
        Log.d(TAG, "newInstance ");
        return new AllControlsFragment();
    }

    public static void replace(int resource, FragmentManager fm) {
        Log.d(TAG, "replace " + resource);
        fm.beginTransaction()
                .replace(resource, newInstance(), AllControlsFragment.TAG)
                .commit();
    }

    private ViewPager mViewPager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView ");
        View view = inflater.inflate(R.layout.fragment_all_controls, container, false);

        mViewPager = (ViewPager) view.findViewById(R.id.vp_controls);
        mViewPager.setAdapter(new AllControlsPagerAdapter(getFragmentManager()));
        mViewPager.setCurrentItem(UserSetting.getCurrentControlPage());

        return view;

    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        UserSetting.setCurrentControlPage(mViewPager.getCurrentItem());
        super.onStop();
    }

    public class AllControlsPagerAdapter extends FragmentStatePagerAdapter {

        public AllControlsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ControlFragmentUtils.getFragment(getActivity().getApplicationContext(), position);
        }

        @Override
        public int getCount() {
            return ControlFragmentUtils.getCount();
        }
    }
}
