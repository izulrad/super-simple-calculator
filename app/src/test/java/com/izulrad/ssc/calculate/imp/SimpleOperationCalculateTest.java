package com.izulrad.ssc.calculate.imp;

import com.izulrad.ssc.calculate.AbstractOperationCalculateTest;

import org.junit.Before;

public class SimpleOperationCalculateTest extends AbstractOperationCalculateTest {
  @Before
  public void setUp() throws Exception {
    operationCalculate = new SimpleOperationCalculate();
    delta = 0.000001d;
  }
}