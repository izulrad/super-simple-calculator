package com.izulrad.ssc.calculate;

import com.izulrad.ssc.util.Log;
import com.izulrad.ssc.util.UserSetting;

import java.math.BigDecimal;

public class CalculateUtils {
    private static final String TAG = CalculateUtils.class.getName();

    /**
     * Trigonometric measures
     */
    public static final int RADIANS = 0;
    public static final int DEGREES = 1;
    public static final int GRADES = 2;
    public static final int MINUTES = 3;
    public static final int SECONDS = 4;

    /**
     * Constants
     */
    public static final double RADIANS_IN_GRADE = Math.PI / 200;
    public static final double RADIANS_IN_MINUTE = 0.000290888209;
    public static final double RADIANS_IN_SECOND = 0.00000484813681;

    public static String openParentheses(String expression) {
        Log.d(TAG, "openParentheses for " + expression);
        if (!expression.startsWith("(")) return expression;
        else return expression.substring(1, expression.length() - 1);
    }

    /**
     * Double to string, without a scientific notation
     * Wrap a negative numbers in a parentheses
     * Scale and Rounding according a user setting
     */
    public static String doubleToString(Double d) {
        Log.d(TAG, "doubleToString " + d);
        if (d.isInfinite() || d.isNaN()) return d.toString();

        BigDecimal result = new BigDecimal(d)
                .setScale(UserSetting.getCurrentNumberScale(), UserSetting.getCurrentRoundingMode())
                .stripTrailingZeros();

        if (result.signum() == 0) return "0"; // 0.000000 fixed in java 8
        return result.signum() < 0 ? "(" + result.toPlainString() + ")" : result.toPlainString();
    }

    /**
     * Find the parenthesis, which a pair, the parenthesis at index in expression
     *
     * @param expression
     * @param index      - index of character, should be close or open parenthesis "(" or ")"
     * @return -1 if do not find
     */
    public static int findPairForParenthesis(String expression, int index) {
        Log.d(TAG, "findPairForParenthesis for " + expression + " start " + index);
        char target = expression.charAt(index);
        switch (target) {
            case '(':
                return findParenthesis(expression, index);
            case ')':
                String reverseExpression = new StringBuffer(expression).reverse().toString();
                int reverseIndex = expression.length() - 1 - index;
                int reverseResult = findParenthesis(reverseExpression, reverseIndex);
                return reverseResult == -1 ? -1 : expression.length() - 1 - reverseResult;
            default:
                return -1;
        }
    }

    private static int findParenthesis(String expression, int index) {
        char target = expression.charAt(index);
        int count = 0;
        for (int i = index; i < expression.length(); i++) {
            char c = expression.charAt(i);

            if (c == '(' || c == ')') {
                if (c == target) count++;
                else count--;
                if (count == 0) return i;
            }
        }
        return -1;
    }

    /**
     * Replace: "**" to "^";
     */
    public static String fixExpression(String expression) {
        return expression
                .replaceAll("\\*\\*", "^");
    }
}
