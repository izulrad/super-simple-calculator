package com.izulrad.ssc.util;


import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ViewUtils {
    private static final String TAG = ViewUtils.class.getName();

    public static void setTextAndSelectTextAndShowKeyboard(final Context context,
                                                           final EditText editText, String text) {
        Log.d(TAG, "setTextAndSelectTextAndShowKeyboard");
        editText.setText(text);
        editText.setSelection(0, text.length());
        editText.requestFocus();
        editText.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager imm = (InputMethodManager)
                        context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, 0);
            }
        }, 200);
    }

    /**
     * @param color
     * @param text
     * @param strings - what should be high
     * @return text with highlight all param strings
     */
    public static Spannable getHighLightText(String text, int color, String... strings) {
        Spannable raw = new SpannableString(text);
        for (String s : strings) {
            Matcher matcher = Pattern.compile(s).matcher(text);
            while (matcher.find()) {
                raw.setSpan(new BackgroundColorSpan(color), matcher.start(), matcher.end(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        return raw;
    }
}
