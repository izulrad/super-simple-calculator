package com.izulrad.ssc.util;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import com.izulrad.ssc.R;
import com.izulrad.ssc.calculate.CalculateController;
import com.izulrad.ssc.view.fragment.ControlFragment;

import java.lang.reflect.Field;
import java.util.HashMap;

public class ControlFragmentUtils {
    private static final String TAG = ControlFragmentUtils.class.getName();
    private static final String CONTROL_FRAGMENT_PREFIX = "fragment_control_";
    private static int controlFragmentCount;
    private static final HashMap<String, ActionHandler> actions = new HashMap<>();

    /**
     * Get count layouts which start with CONTROL_FRAGMENT_PREFIX
     */
    public static int getCount() {
        if (controlFragmentCount == 0) {
            for (Field f : R.layout.class.getFields()) {
                if (f.getName().startsWith(CONTROL_FRAGMENT_PREFIX)) controlFragmentCount++;
            }
        }
        return controlFragmentCount;
    }

    public static Fragment getFragment(Context context, int position) {
        Log.d(TAG, "getFragment " + position);

        int layoutId = context.getResources().getIdentifier(CONTROL_FRAGMENT_PREFIX + position,
                "layout", context.getPackageName());

        return ControlFragment.newInstance(layoutId);
    }

    public static void initStringResources(final Context context) {
        Log.d(TAG, "initStringResources ");

        actions.put(context.getString(R.string.btn_back_space), new ActionHandler() {
            @Override
            public void run() {
                DisplayUtils.deleteCharacterBeforeCursorPosition();
            }
        });

        actions.put(context.getString(R.string.btn_clear), new ActionHandler() {
            @Override
            public void run() {
                DisplayUtils.clear();
            }
        });

        actions.put(context.getString(R.string.btn_equal), new ActionHandler() {
            @Override
            public void run() {

                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        DisplayUtils.setText(CalculateController.calc(context, DisplayUtils.getText()));
                        DisplayUtils.moveCursorToEndPosition();
                        return null;
                    }
                }.doInBackground();

            }
        });

        actions.put(context.getString(R.string.btn_left), new ActionHandler() {
            @Override
            public void run() {
                DisplayUtils.moveCursorToLeft();
            }
        });

        actions.put(context.getString(R.string.btn_right), new ActionHandler() {
            @Override
            public void run() {
                DisplayUtils.moveCursorToRight();
            }
        });

        actions.put(context.getString(R.string.parentheses), new ActionHandler() {
            @Override
            public void run() {
                DisplayUtils.appendTextOnCursorPosition("()");
                DisplayUtils.moveCursorToLeft();
            }
        });
    }

    public static void click(String buttonText) {
        Log.d(TAG, "click " + buttonText);

        ActionHandler handler = actions.get(buttonText);
        if (handler != null) handler.run();
        else DisplayUtils.appendTextOnCursorPosition(buttonText);
    }

    private interface ActionHandler {
        void run();
    }
}
