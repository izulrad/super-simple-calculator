package com.izulrad.ssc.calculate.exception;

public class OperandNotDefinedException extends RuntimeException {
    public OperandNotDefinedException(String detailMessage) {
        super(detailMessage);
    }
}
