package com.izulrad.ssc.calculate;

public interface ExpressionCalculate {
    String calculate(String expression);
}
