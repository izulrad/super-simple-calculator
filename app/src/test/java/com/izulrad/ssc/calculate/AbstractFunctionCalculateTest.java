package com.izulrad.ssc.calculate;

import com.izulrad.ssc.util.Log;
import com.izulrad.ssc.util.UserSetting;

import org.junit.Test;

import static com.izulrad.ssc.calculate.CalculateUtils.DEGREES;
import static com.izulrad.ssc.calculate.CalculateUtils.GRADES;
import static com.izulrad.ssc.calculate.CalculateUtils.MINUTES;
import static com.izulrad.ssc.calculate.CalculateUtils.RADIANS;
import static com.izulrad.ssc.calculate.CalculateUtils.SECONDS;
import static com.izulrad.ssc.calculate.CalculateUtils.openParentheses;
import static org.junit.Assert.assertEquals;

abstract public class AbstractFunctionCalculateTest {
  protected FunctionCalculate functionCalculate;
  protected Double delta;

  /**
   * Sequence the testing data: function[0] argument[1] =[2] result[3]
   * Split of " "
   */
  private String[] functions = {
      "abs -1 = 1",
      "lg 10 = 1"
  };

  /**
   * Sequence the testing data:
   * function[0] argument[1] =[2] results_in radians[3] degrees[4] grades[5] minutes[6] seconds[7]
   * Split of " "
   */
  private String[] tFunctions = {
      "sin 0 = 0 0",
      "cos 0 = 1 1",
      "sin 361 = 0.2793865544 0.0174524064 -0.5750052520 0.1048177535 0.0017501765"
  };

  @Test
  public void testFunction() throws Exception {
    if (functionCalculate == null)
      throw new NullPointerException("functionCalculate == null, use setUp method");
    if (delta == null) throw new NullPointerException("delta == null, use setUp method");
    UserSetting.loadDefault();
    Log.setDebugMode(false);

    for (String s : functions) {
      String[] arr = s.split(" ");
      String function = arr[0];
      String arg = arr[1];
      Double expected = Double.valueOf(arr[3]);
      Double actual = Double.valueOf(openParentheses(functionCalculate.function(function, arg)));

      assertEquals(String.format("function %s(%s) = ?", function, arg), expected, actual, delta);
    }

    for (String s : tFunctions) {
      String[] arr = s.split(" ");
      String function = arr[0];
      String arg = arr[1];
      Double expected;
      Double actual;

      //RADIAN
      UserSetting.setCurrentTrigonometricMeasure(RADIANS);
      expected = Double.valueOf(arr[3]);
      actual = Double.valueOf(openParentheses(functionCalculate.function(function, arg)));
      assertEquals(String.format("RADIAN %s(%s) = ?", function, arg), expected, actual, delta);

      //DEGREES
      if (arr.length > 4) {
        UserSetting.setCurrentTrigonometricMeasure(DEGREES);
        expected = Double.valueOf(arr[4]);
        actual = Double.valueOf(openParentheses(functionCalculate.function(function, arg)));
        assertEquals(String.format("DEGREES %s(%s) = ?", function, arg), expected, actual, delta);
      }
      //GRADES
      if (arr.length > 5) {
        UserSetting.setCurrentTrigonometricMeasure(GRADES);
        expected = Double.valueOf(arr[5]);
        actual = Double.valueOf(openParentheses(functionCalculate.function(function, arg)));
        assertEquals(String.format("GRADES %s(%s) = ?", function, arg), expected, actual, delta);
      }

      //MINUTES
      if (arr.length > 6) {
        UserSetting.setCurrentTrigonometricMeasure(MINUTES);
        expected = Double.valueOf(arr[6]);
        actual = Double.valueOf(openParentheses(functionCalculate.function(function, arg)));
        assertEquals(String.format("MINUTES %s(%s) = ?", function, arg), expected, actual, delta);
      }

      //SECONDS
      if (arr.length > 7) {
        UserSetting.setCurrentTrigonometricMeasure(SECONDS);
        expected = Double.valueOf(arr[7]);
        actual = Double.valueOf(openParentheses(functionCalculate.function(function, arg)));
        assertEquals(String.format("SECONDS %s(%s) = ?", function, arg), expected, actual, delta);
      }
    }
  }
}
