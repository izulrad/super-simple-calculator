package com.izulrad.ssc.calculate;

import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.StyleSpan;

import com.izulrad.ssc.R;
import com.izulrad.ssc.calculate.imp.RecursiveExpressionCalculate;
import com.izulrad.ssc.calculate.imp.SimpleExpressionCheck;
import com.izulrad.ssc.calculate.imp.SimpleFunctionCalculate;
import com.izulrad.ssc.calculate.imp.SimpleOperationCalculate;
import com.izulrad.ssc.calculate.imp.SimplePlaceParentheses;
import com.izulrad.ssc.util.HistoryUtils;

import java.util.List;

public class CalculateController {

    private static ExpressionCalculate expressionCalculate = new RecursiveExpressionCalculate(
            new SimpleOperationCalculate(), new SimpleFunctionCalculate());
    private static ExpressionCheck expressionCheck = new SimpleExpressionCheck();
    private static PlaceParentheses placeParentheses = new SimplePlaceParentheses();

    public static Spannable calc(final Context context, final String expression) {
        String fixedExp = CalculateUtils.fixExpression(expression);
        Spannable result;

        List<Segment> illegalSegments = expressionCheck.check(fixedExp);

        if (illegalSegments.size() > 0) {
            result = new SpannableString(fixedExp);
            for (Segment s : illegalSegments) {
                result.setSpan(
                        new BackgroundColorSpan(context.getResources().getColor(R.color.scc_illegal_expression_segment)),
                        s.start, s.end,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        } else {
            try {
                String formattedExpression = placeParentheses.place(fixedExp);
                String calculateResult = expressionCalculate.calculate(formattedExpression);
                HistoryUtils.add(formattedExpression, calculateResult);
                result = new SpannableString(calculateResult);
                result.setSpan(
                        new StyleSpan(Typeface.BOLD),
                        0, result.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            } catch (Throwable t) {
                result = new SpannableString("ERROR");
            }
        }

        return result;
    }
}