package com.izulrad.ssc.util;

import android.content.Context;

import com.izulrad.ssc.R;

import java.util.LinkedHashMap;
import java.util.Map;

public class HistoryUtils {
    private static final String TAG = HistoryUtils.class.getName();
    private static Map<String, String> HISTORY = new LinkedHashMap<>();

    public static void init(Context context, String history) {
        Log.d(TAG, "init ");
        if (history.length() > 0) {
            String[] strings = history.split(context.getString(R.string.setting_delimiter));
            for (int i = strings.length - 1; i >= 0; i--) {
                String s = strings[i];
                String[] historyItem = s.split("=");
                HISTORY.put(historyItem[0], historyItem[1]);
            }
        }
    }

    public static String concat(Context context) {
        Log.d(TAG, "concat ");
        StringBuilder result = new StringBuilder();
        int size = UserSetting.getCurrentHistorySize() <= HISTORY.size()
                ? UserSetting.getCurrentHistorySize() : HISTORY.size();

        for (int i = 0; i < size; i++) {
            Map.Entry<String, String> entry = get(i);
            result
                    .append(entry.getKey())
                    .append("=")
                    .append(entry.getValue())
                    .append(context.getString(R.string.setting_delimiter));
        }
        Log.d(TAG, "concat result " + result.toString());
        return result.toString();
    }

    public static void clear() {
        Log.d(TAG, "clear ");
        HISTORY.clear();
    }

    public static void add(String expression, String result) {
        if (HISTORY.containsKey(expression)) HISTORY.remove(expression);
        HISTORY.put(expression, result);
    }

    public static void remove(String expression) {
        Log.d(TAG, "remove " + expression);
        HISTORY.remove(expression);
    }

    public static String getResult(String expression) {
        return HISTORY.get(expression);
    }

    public static int getSize() {
        return HISTORY.size();
    }

    @SuppressWarnings("unchecked")
    public static Map.Entry<String, String> get(int position) {
        return (Map.Entry<String, String>) HISTORY.entrySet().toArray()[HISTORY.size() - 1 - position];
    }
}
