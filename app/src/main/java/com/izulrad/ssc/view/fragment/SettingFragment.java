package com.izulrad.ssc.view.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.izulrad.ssc.R;
import com.izulrad.ssc.util.Log;

public class SettingFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = SettingFragment.class.getName();

    public static Fragment newInstance() {
        Log.d(TAG, "getInstance ");
        return new SettingFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView ");
        View view = inflater.inflate(R.layout.fragment_setting, container, false);

        View[] buttons = {
                view.findViewById(R.id.btn_setting_btn_text_size),
                view.findViewById(R.id.btn_setting_display_text_size),
                view.findViewById(R.id.btn_setting_history_size),
                view.findViewById(R.id.btn_setting_number_scale),
                view.findViewById(R.id.btn_setting_rounding_mode),
                view.findViewById(R.id.btn_setting_trigonometric_measure),
        };

        for (View v : buttons) {
            v.setOnClickListener(this);
        }


        return view;
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick " + v);

        switch (v.getId()) {
            case R.id.btn_setting_btn_text_size:
                Log.d(TAG, "onClick " + "R.id.btn_setting_btn_text_size");
                break;
            case R.id.btn_setting_display_text_size:
                Log.d(TAG, "onClick " + "R.id.btn_setting_display_text_size");
                break;
            case R.id.btn_setting_history_size:
                Log.d(TAG, "onClick " + "R.id.btn_setting_history_size");
                break;
            case R.id.btn_setting_number_scale:
                Log.d(TAG, "onClick " + "R.id.btn_setting_number_scale");
                break;
            case R.id.btn_setting_rounding_mode:
                Log.d(TAG, "onClick " + "R.id.btn_setting_rounding_mode");
                break;
            case R.id.btn_setting_trigonometric_measure:
                Log.d(TAG, "onClick " + "R.id.btn_setting_trigonometric_measure");
                break;
        }
    }
}
