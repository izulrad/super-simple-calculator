package com.izulrad.ssc.util;

import android.content.Context;
import android.text.Editable;
import android.text.Spannable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.izulrad.ssc.R;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class ConstantUtils {
    private static final String TAG = ConstantUtils.class.getName();
    private static final Map<String, String> CONSTANTS = new TreeMap<>();

    public static void init(Context context, String constants) {
        CONSTANTS.putAll(parse(context, constants));
    }

    public static void restoreDefault(Context context) {
        init(context, context.getString(R.string.constants_default));
    }

    private static Map<String, String> parse(Context context, String constants) {
        Map<String, String> result = new HashMap<>();
        for (String s : constants.split(context.getString(R.string.setting_delimiter))) {
            String[] arr = s.split("=");
            result.put(arr[0], arr[1]);
        }
        return result;
    }

    public static void addNewConstant(String name, String value) {
        Log.d(TAG, String.format("addNewConstant %s = %s", name, value));
        CONSTANTS.put(name, value);
    }

    public static void delete(String name) {
        Log.d(TAG, "delete " + name);
        CONSTANTS.remove(name);
    }

    public static boolean hasName(String name) {
        Log.d(TAG, "hasName " + name);
        return CONSTANTS.containsKey(name);
    }

    public static String getValue(String name) {
        Log.d(TAG, "getValue " + name);
        return CONSTANTS.get(name);
    }

    public static void editName(String oldName, String newName) {
        Log.d(TAG, "editName " + oldName + " to " + newName);
        String value = CONSTANTS.get(oldName);
        CONSTANTS.remove(oldName);
        CONSTANTS.put(newName, value);
    }

    public static void editValue(String name, String value) {
        Log.d(TAG, "editValue key " + name + " value " + value);
        CONSTANTS.put(name, value);
    }

    @SuppressWarnings("unchecked")
    public static Map.Entry<String, String> get(int position) {
        Log.d(TAG, "get " + position);
        return (Map.Entry<String, String>) CONSTANTS.entrySet().toArray()[position];
    }

    public static int getSize() {
        Log.d(TAG, "getSize ");
        return CONSTANTS.size();
    }

    /**
     * Concat all constant to string with delimiter
     */
    public static String concat(Context context) {
        StringBuilder result = new StringBuilder();
        for (Map.Entry<String, String> entry : CONSTANTS.entrySet()) {
            result
                    .append(entry.getKey())
                    .append("=")
                    .append(entry.getValue())
                    .append(context.getString(R.string.setting_delimiter));
        }
        return result.toString();
    }

    /**
     * Verify constant name or value for length == 0 or contain delimiter
     *
     * @return false if verify failed
     */
    public static boolean verify(Context context, EditText editText, boolean isName) {
        Log.d(TAG, "verify " + editText);
        boolean result = true;
        String text = editText.getText().toString();

        if (text.length() == 0) {
            editText.setError(context.getString(R.string.dialog_constant_error_should_not_to_be_empty));
            result = false;
        }

        String constantDelimiter = context.getString(R.string.setting_delimiter);
        if (text.contains(constantDelimiter)) {
            Spannable lightText = ViewUtils.getHighLightText(text,
                    context.getResources().getColor(R.color.scc_illegal_character_back_ground),
                    constantDelimiter);
            editText.setText(lightText);
            editText.setSelection(text.length());
            editText.setError(context.getString(R.string.dialog_constant_error_illegal_character));
            result = false;
        }

        if (isName && ConstantUtils.hasName(text)) {
            editText.setError(context.getString(R.string.dialog_constant_error_same_name));
            result = false;
        }

        if (!result) removeErrAfterTextChanged(editText);

        return result;
    }

    private static void removeErrAfterTextChanged(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                editText.setError(null);
            }
        });
    }
}
