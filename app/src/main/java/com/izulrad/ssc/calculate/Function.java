package com.izulrad.ssc.calculate;

import com.izulrad.ssc.util.Log;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * name, isTrigonometric?, signs
 */
public enum Function {
    ABS("abs", false, "abs"),
    LN("ln", false, "ln", "loge"),
    LG("lg", false, "lg", "log10"),
    SIN("sin", true, "sin"),
    COS("cos", true, "cos"),
    TAN("tan", true, "tan"),
    CTAN("ctan", true, "ctan"),
    SEC("sec", true, "sec"),
    COSEC("cosec", true, "cosec"),
    ASIN("asin", true, "asin"),
    ACOS("acos", true, "acos"),
    ATAN("atan", true, "atan"),
    SINH("sinh", true, "sinh"),
    COSH("cosh", true, "cosh"),
    TANH("tanh", true, "tanh");

    private static final String TAG = Function.class.getName();

    public static Function getFunctionForSign(String sign) {
        for (Function f : Function.values()) {
            if (f.signs.contains(sign)) return f;
        }

        throw new NoSuchElementException("Have no function with sign " + sign);
    }

    public static boolean hasFunction(String function) {
        Log.d(TAG, "hasFunction " + function);
        for (Function f : Function.values()) {
            if (f.signs.contains(function)) return true;
        }

        return false;
    }

    private String name;
    private boolean isTrigonometric;
    private List<String> signs;

    Function(String name, boolean isTrigonometric, String... signs) {
        this.name = name;
        this.isTrigonometric = isTrigonometric;
        this.signs = Arrays.asList(signs);
    }

    public String getName() {
        return name;
    }

    public boolean isTrigonometric() {
        return isTrigonometric;
    }
}
